## Die Website von kielkannmehr.de

Nach dem Kompilieren mit `yarn build` befindet sich die statische Website im Ordner `dist`.

Markdown Inhalte befinden sich in `public/content`.
