import React from 'react'
import CSS from 'csstype'
import { TOP_MARGIN } from '../../common/constants'

type Props = React.PropsWithChildren<{
    style?: CSS.Properties
}>

export const Content = (props: Props) => (
    <div style={{ ...style, ...props.style }}>{props.children}</div>
)

const style: CSS.Properties = {
    position: 'relative',
    flex: 1,
    height: '100%',
    width: '100%',
    maxWidth: '800px',
    background: 'white',
    top: 0,
    left: '156px',
    right: 0,
    bottom: 0,
    paddingTop: `${TOP_MARGIN}px`,
    paddingLeft: '96px',
    paddingRight: '96px',
    overflow: 'overlay',
}
