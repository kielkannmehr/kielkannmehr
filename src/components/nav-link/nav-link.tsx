/// <reference path='../../../index.d.ts' />
import { Link } from '@reach/router'
import CSS from 'csstype'
import React from 'react'

import IconC from '../../../public/assets/icons/icon-c.png'
import IconSquare from '../../../public/assets/icons/icon-square.png'
import IconTriplus from '../../../public/assets/icons/icon-triplus.png'
import IconRightArrow from '../../../public/assets/icons/icon-right-arrow.png'
import { IconSource } from 'types/icon-source'

interface ExternalProps extends React.ComponentProps<typeof Link> {
    src: IconSource
    iconStyle?: CSS.Properties
}

export type Props = ExternalProps

export const NavLink = (props: Props) => (
    <Link style={{ ...style, ...props.style }} {...props}>
        <img
            style={{
                ...iconStyle,
                ...props.iconStyle,
            }}
            width={36}
            height={36}
            src={mapSrcToIcon(props.src)}
        ></img>
        <div style={{ zIndex: 2 }}>{props.children}</div>
    </Link>
)

const mapSrcToIcon = (src: IconSource) => {
    switch (src) {
        case 'right-arrow':
            return IconRightArrow
        case 'square':
            return IconSquare
        case 'triplus':
            return IconTriplus
        case 'c':
        default:
            return IconC
    }
}

const iconStyle: CSS.Properties = {
    marginLeft: '12px',
    marginRight: '12px',
    position: 'relative',
    height: '50px',
    width: '50px',
}

const style: CSS.Properties = {
    flexDirection: 'row',
    color: 'black',
    display: 'flex',
    flex: 1,
    justifyItems: 'center',
    alignItems: 'center',
    paddingLeft: '28px',
    fontSize: '25pt',
    fontWeight: 'bold',
}
