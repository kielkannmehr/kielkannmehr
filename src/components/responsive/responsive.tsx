import React from 'react'
import { useIsMobile } from '../../hooks'

type Props = React.PropsWithChildren<any>

export const Desktop = ({ children }: Props) => {
    const isMobile = useIsMobile()
    return !isMobile ? children : null
}

export const Mobile = ({ children }: Props) => {
    const isMobile = useIsMobile()
    return isMobile ? children : null
}

export const Default = Desktop
