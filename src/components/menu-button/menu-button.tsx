/// <reference path='../../../index.d.ts' />
import React from 'react'
import { useSpring, animated } from 'react-spring'
import { easeSinInOut } from 'd3-ease'
import { CSS } from '../../types/css'
import Icon from '../../../public/assets/icons/menu.svg'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../redux/index'
import { SideMenuAction } from '../../redux/side-menu'

type ExternalProps = {
    style: CSS.Properties
}

type Props = ExternalProps

const useAnimation = (): [CSS.Properties, () => void] => {
    const dispatch = useDispatch()
    const isOpen = useSelector((state: RootState) => state.sideMenu.isOpen)

    const style = useSpring({
        to: { transform: isOpen ? 'rotate(-90deg)' : 'rotate(0deg)' },
        config: { duration: 450, easing: easeSinInOut, clamp: false },
    })

    const toggle = () => dispatch(SideMenuAction.toggle())

    return [style, toggle]
}

export const MenuButton = (props: Props) => {
    const [animationStyle, toggle] = useAnimation()

    return (
        <div style={{ ...style, ...props.style }} onClick={toggle}>
            <animated.div style={animationStyle}>
                <Icon width={50} height={50} key={'icon'} />
            </animated.div>
        </div>
    )
}

const style: CSS.Properties = {
    padding: '12px',
    backgroundColor: 'white',
    borderColor: 'transparent',
    height: 'auto',
}
