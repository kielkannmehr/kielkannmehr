import React from 'react'
import ReactMarkdown from 'react-markdown'

interface ExternalProps {
    markdown: string
}

export const Markdown = (props: ExternalProps) => {
    return (
        <div className={'markdown'}>
            <ReactMarkdown
                transformImageUri={(uri) => uri.replace('../', '')}
                source={props.markdown}
            />
        </div>
    )
}
