import React from 'react'
import CSS from 'csstype'
import { NavLink } from '../nav-link'
import { RootState } from '../../redux'
import { useSelector } from 'react-redux'
import { useSpring, animated } from 'react-spring'
import { easeSinInOut } from 'd3-ease'
import { drawerRouteData } from 'components/navigation/types'
import { useIsMobile } from '../../hooks'
import { SIDE_MENU_WIDTH, TOP_MARGIN } from '../../common/constants'

type Props = React.PropsWithChildren<{
    style?: CSS.Properties | undefined
}>

const useAnimation = (isOpen: boolean) => {
    return useSpring({
        marginLeft: isOpen ? 0 : -SIDE_MENU_WIDTH,
        config: {
            duration: 450,
            easing: easeSinInOut,
            clamp: false,
        },
    })
}

export const Navigation = (props: Props) => {
    const isOpen = useSelector((state: RootState) => state.sideMenu.isOpen)
    const isMobile = useIsMobile()

    const animationStyle = useAnimation(isOpen)

    return (
        <animated.div
            style={{
                ...style,
                position: isMobile ? 'absolute' : 'fixed',
                ...props.style,
                ...animationStyle,
            }}
        >
            <nav>
                {drawerRouteData.map(({ title, route, icon }) => (
                    <NavLink src={icon} to={route}>
                        {title}
                    </NavLink>
                ))}
                {props.children}
            </nav>
        </animated.div>
    )
}

const style: CSS.Properties = {
    paddingTop: `${TOP_MARGIN}px`,
    flexDirection: 'column',
    flex: 1,
    backgroundColor: 'white',
    width: `${SIDE_MENU_WIDTH}px`,
    minHeight: '100vh',
}
