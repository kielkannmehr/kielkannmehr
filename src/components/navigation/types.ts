import { IconSource } from '../../types/icon-source'

enum Route {
    Home = '/',
    Program = '/program',
    Concept = '/concept',
    Map = '/map',
    People = '/people',
    Sponsors = '/sponsors',
    About = '/about',
    Current = '/current',
}

export interface RouteData {
    title: string
    route: Route
}

export interface DrawerRouteData extends RouteData {
    icon: IconSource
}

const home: RouteData = {
    title: 'Home',
    route: Route.Home,
}

const program: DrawerRouteData = {
    title: 'Programm',
    route: Route.Program,
    icon: 'triangle',
}

const concept: DrawerRouteData = {
    title: 'Konzept',
    route: Route.Concept,
    icon: 'c',
}

const map: DrawerRouteData = {
    title: 'Karte',
    route: Route.Map,
    icon: 'right-arrow',
}

const people: DrawerRouteData = {
    title: 'Personen',
    route: Route.People,
    icon: 'square',
}

const sponsors: DrawerRouteData = {
    title: 'Sponsoren',
    route: Route.Sponsors,
    icon: 'triplus',
}

const about: DrawerRouteData = {
    title: 'Impressum',
    route: Route.About,
    icon: 'ring',
}

const current: DrawerRouteData = {
    title: 'Aktuelles',
    route: Route.Current,
    icon: 'right-arrow',
}

export const drawerRouteData: DrawerRouteData[] = [
    // program,
    concept,
    //map,
    current,
    people,
    sponsors,
    about,
]

export const RouteData = {
    home,
    program,
    concept,
    map,
    current,
    people,
    sponsors,
    about,
}
