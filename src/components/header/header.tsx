import React from 'react'
import CSS from 'csstype'

export const Header = (props: React.PropsWithChildren<{}>) => (
    <div style={style}> {props.children}</div>
)

const style : CSS.Properties = {
    flex: 1,
    flexDirection: 'row'
}