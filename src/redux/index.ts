import { createStore, combineReducers } from 'redux'
import { reducer as sideMenuReducer } from './side-menu'

const reducer = combineReducers({ sideMenu: sideMenuReducer })

export type RootState = ReturnType<typeof reducer>

export const store = createStore(reducer)
