interface SideMenuState {
    isOpen: boolean
}

const initialState: SideMenuState = { isOpen: false }

type SideMenuAction = { type: 'toggle' | 'open' | 'close' }

export const SideMenuAction = {
    toggle: (): SideMenuAction => ({
        type: 'toggle',
    }),
    open: (): SideMenuAction => ({
        type: 'open',
    }),
    close: (): SideMenuAction => ({
        type: 'close',
    }),
}

export const reducer = (
    state: SideMenuState = initialState,
    action: SideMenuAction
): SideMenuState => {
    switch (action.type) {
        case 'toggle':
            return { isOpen: !state.isOpen }
        case 'close':
            return { isOpen: false }
        case 'open':
            return { isOpen: true }
        default:
            return state
    }
}
