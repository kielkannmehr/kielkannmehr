import React from 'react'
import { CSS } from '../types/css'

export default () => (
    <div style={style}>
        <h1 style={h1}>KIEL KANN MEHR</h1>
        <h2 style={h2}>ZUKUNFTSFESTIVAL</h2>
        <h1 style={h1}>05.06.2020</h1>
    </div>
)

const h1: CSS.Properties = {
    marginTop: '0px',
    marginBottom: '0px',
    lineHeight: '129pt',
    fontWeight: 'bold',
    fontSize: '78pt',
}

const h2: CSS.Properties = {
    marginTop: '0px',
    lineHeight: '129pt',
    marginBottom: '0px',
    fontWeight: 'normal',
    fontSize: '78pt',
}

const style: CSS.Properties = {
    paddingLeft: '150px',
    paddingTop: '50px',
    paddingBottom: '95px',
    marginTop: '20vh',
    marginLeft: '20vh',
    textAlign: 'left',
    backgroundColor: 'white',
    alignSelf: 'flex-end',
    justifySelf: 'flex-end',
    width: '100%',
    height: 'auto',
}
