/// <reference path='../../index.d.ts' />
import React from 'react'
import { Markdown } from 'components/markdown'
import about from '../../public/content/about.md'
import { Content } from 'components/content'

export default () => {
    return (
        <Content>
            <Markdown markdown={about} />
        </Content>
    )
}
