/// <reference path='../../index.d.ts' />
import React from 'react'
import { Markdown } from 'components/markdown'
import concept from '../../public/content/concept.md'
import { Content } from 'components/content'
import logo from '../../public/assets/img/logo/logo.png'

export default () => {
    return (
        <Content>
            <img style={{ float: 'right' }} src={logo} width={'256px'} />
            <Markdown markdown={concept} />
        </Content>
    )
}
