/// <reference path='../../index.d.ts' />
import React from 'react'
import { Markdown } from 'components/markdown'
import people from '../../public/content/people.md'
import { Content } from 'components/content'

export default () => {
    return (
        <Content>
            <Markdown markdown={people} />
        </Content>
    )
}
