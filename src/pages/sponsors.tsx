/// <reference path='../../index.d.ts' />
import React from 'react'
import { Markdown } from 'components/markdown'
import sponsors from '../../public/content/sponsors.md'
import { Content } from 'components/content'
import CSS from 'csstype'

import anscharcampus from '../../public/assets/img/logo/logo-anscharcampus.png'
import beiSh from '../../public/assets/img/logo/logo-bei-sh.png'
import cau from '../../public/assets/img/logo/logo-cau.png'
import fh from '../../public/assets/img/logo/logo-fh-kiel.png'
import geomar from '../../public/assets/img/logo/logo-geomar.png'
import hbssh from '../../public/assets/img/logo/logo-hbs-sh.png'
import kielregion from '../../public/assets/img/logo/logo-kielregion.png'
import stadtkiel from '../../public/assets/img/logo/logo-stadtkiel.png'
import klimaschutzstadt from '../../public/assets/img/logo/logo-klimaschutzstadt.png'
import stadtnatur from '../../public/assets/img/logo/logo-stadtnatur.png'

const logos = [
    [anscharcampus, 'anscharcampus'],
    [beiSh, 'bei-sh'],
    [cau, 'cau'],
    [fh, 'fh'],
    [geomar, 'geomar'],
    [hbssh, 'hbssh'],
    [kielregion, 'kielregion'],
    [klimaschutzstadt, 'klimaschutzstadt'],
    [stadtkiel, 'stadtkiel'],
    [stadtnatur, 'stadtnatur'],
].sort(([_, a], [__, b]) => a.localeCompare(b))

export default () => {
    return (
        <Content>
            <Markdown markdown={sponsors} />
            <div style={logosTableStyle}>
                {logos.map(([logo, label]) => (
                    <img style={imgStyle} src={logo} alt={label} />
                ))}
            </div>
        </Content>
    )
}

const logosTableStyle: CSS.Properties = {
    marginTop: '90px',
}

const imgStyle: CSS.Properties = {
    marginBottom: '15px',
    marginRight: '30px',
}
