/// <reference path='../../index.d.ts' />
import React from 'react'
import { Markdown } from 'components/markdown'
import current from '../../public/content/current.md'
import { Content } from 'components/content'

export default () => {
    return (
        <Content>
            <Markdown markdown={current} />
        </Content>
    )
}
