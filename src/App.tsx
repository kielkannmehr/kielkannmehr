import React from 'react'
import { Root, Routes } from 'react-static'
import { Router } from '@reach/router'
import 'normalize.css'
import './app.css'
import { useSpring, animated } from 'react-spring'
import { Navigation } from './components/navigation'
import { Row } from './containers/row'
import { MenuButton } from './components/menu-button'
import { RootState, store } from './redux'
import { Provider, useSelector } from 'react-redux'
import CSS from 'csstype'
import { easeSinInOut } from 'd3-ease'
import { SIDE_MENU_WIDTH } from './common/constants'

const App = () => {
    return (
        <Provider store={store}>
            <Root>
                <MenuButton
                    style={{
                        zIndex: 10,
                        position: 'fixed',
                        top: '65px',
                        left: '55px',
                    }}
                />
                <Row
                    style={{
                        flex: 1,
                        height: '100%',
                        alignItems: 'spread',
                    }}
                >
                    <Navigation />
                    <Content />
                </Row>
            </Root>
        </Provider>
    )
}

const useContentAnimation = (isOpen: Boolean) => {
    return useSpring({
        marginLeft: isOpen ? SIDE_MENU_WIDTH : 0,
        config: {
            duration: 450,
            easing: easeSinInOut,
            clamp: false,
        },
    })
}

const Content = () => {
    const isOpen = useSelector((state: RootState) => state.sideMenu.isOpen)
    const animationStyle = useContentAnimation(isOpen)

    return (
        <React.Suspense fallback={<em>Loading...</em>}>
            <animated.div
                style={{
                    ...animationStyle,
                    height: '100%',
                }}
            >
                <Router>
                    <Routes path="*" />
                </Router>
            </animated.div>
        </React.Suspense>
    )
}

export default App
