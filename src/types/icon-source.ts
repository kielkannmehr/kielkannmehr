export type IconSource =
    | 'c'
    | 'triplus'
    | 'square'
    | 'right-arrow'
    | 'triangle'
    | 'ring'
