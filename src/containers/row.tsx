import React from 'react'
import { CSS } from '../types/css'

type Props = React.PropsWithChildren<{ style?: CSS.Properties }>

export const Row = (props: Props) => (
    <div style={{ ...style, ...props.style }}> {props.children} </div>
)

const style: CSS.Properties = {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    display: 'inline-flex',
}
