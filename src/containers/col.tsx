import React from 'react'
import CSS from 'csstype'
type Props = React.PropsWithChildren<{ style?: CSS.Properties }>

export const Col = (props: Props) => (
    <div style={{ ...style, ...props.style }}> {props.children} </div>
)

const style: CSS.Properties = {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-start',
}
