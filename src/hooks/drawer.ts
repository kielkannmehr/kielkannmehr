import { useDispatch } from 'react-redux'
import { SideMenuAction } from '../redux/side-menu'

interface UseDrawer {
    open: () => void
    close: () => void
    toggle: () => void
}

export const useDrawer = (): UseDrawer => {
    const dispatch = useDispatch()

    return {
        toggle: () => dispatch(SideMenuAction.toggle()),
        open: () => dispatch(SideMenuAction.open()),
        close: () => dispatch(SideMenuAction.close()),
    }
}
