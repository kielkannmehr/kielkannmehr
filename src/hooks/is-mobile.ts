import { useMediaQuery } from 'react-responsive'
import { MOBILE_MAX_WIDTH } from '../common/constants'

export const useIsMobile = () => useMediaQuery({ maxWidth: MOBILE_MAX_WIDTH })
