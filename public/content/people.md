## Personen

Das Kiel Kann Mehr Zukunftsfestival wird organisiert von Studierenden der School of Sustainability an der
Christian-Albrechts-Universität zu Kiel. Im Foto von links oben nach rechts unten: Hauke Dentzin, Mareike Chalkley,
Sabrina Grape, Lisa Vagts, Maura Rafelt, Jonas Mayer

![insert images](../assets/img/photo-group.jpg)

Sie erreichen das Team per E-Mail an info@kielkannmehr.de oder über die
[Facebookseite](https://www.facebook.com/KielKannMehr/).
