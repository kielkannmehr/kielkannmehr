## Konzept

Alle reden von Nachhaltigkeit. Medien berichten davon und junge Menschen gehen dafür auf die Straße. Unternehmen suchen
Innovationen und Politiker\*innen beziehen Stellung. Es gibt viele Initiativen, die mit an einer sozialen, ökologischen
und wirtschaftlich nachhaltigen Welt arbeiten. All das passiert auch in Kiel. Aber was genau? Wer macht das? Und wo ist
Potenzial für mehr? Um die Auseinandersetzung mit einer nachhaltigen Zukunft in Kiel konstruktiv voranzubringen, findet
das Kiel Kann Mehr Zukunftsfestival statt – mit einem für Kiel und darüber hinaus neuen Veranstaltungskonzept. Wir
bringen einen Tag lang Bürger\*innen mit Expert\*innen und Entscheidungsträger\*innen aus Politik, Wirtschaft und Forschung
zusammen, um Impulse für eine ökologisch, sozial und wirtschaftlich nachhaltige Zukunft unserer Stadt zu setzen.
Mitgestalten, Stellung beziehen, sichtbar machen. Kiel Kann Mehr!
