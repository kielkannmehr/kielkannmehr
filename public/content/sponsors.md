## Unterstützer\*innen

Das Kiel Kann Mehr Zukunftsfestival wird von diversen Organisationen, Unternehmen und Behörden unterstützt. Das passiert
durch finanzielle Förderung, Hilfe in der Veranstaltungsplanung, die Bereitstellung von Standorten und durch
Programmpunkte. Vielen Dank dafür!
